#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Author  : li
# @Email   : wytheli168@163.com
# @Time    : 2019/11/20 21:42
# @Description:
from pymongo import MongoClient

import config

client = MongoClient(host=config.HOST, port=config.PORT, username=config.USERNAME, password=config.PASSWORD)
db = client[config.DB]


# class MongoConn(object):
#     def __init__(self, db, collection):
#         self.client = MongoClient(host=config.HOST, port=config.PORT, username=config.USERNAME, password=config.PASSWORD)
#         self.collection = client[db][collection]
