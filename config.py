#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Author  : li
# @Email   : wytheli168@163.com
# @Time    : 2019/11/20 20:35
# @Description:

# mongodb配置
import logging

USERNAME = "root"
PASSWORD = "Meanergy168"
HOST = "139.129.229.223"
PORT = 27017
DB = "mn_sports_qq_nba"

# 爬取指定日期内的直播信息
START_TIME = "2019-10-01"
END_TIME = "2019-11-21"

# 代理池
# PROXIES = {
#     "http": "http://12.34.56.79:9527",
#     "https": "https://12.34.56.79:9527",
# }

# User-Agent池
headers = {
    "User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
}

# Splash服务地址
# splash_url = "http://47.101.37.196:8050/"

# PhantomJS安装位置
PHANTOMJS = r"C:\Users\WytheLi\Downloads\phantomjs-2.1.1-windows\bin\phantomjs.exe"

# 日志等级
# LEVEL = logging.DEBUG
LEVEL = logging.INFO

