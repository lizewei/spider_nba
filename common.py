#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Author  : li
# @Email   : wytheli168@163.com
# @Time    : 2019/11/20 21:39
# @Description:
import logging
import time
import hashlib
import random
from logging.handlers import RotatingFileHandler

import requests
from retrying import retry

import config
from db import db

logger = logging.getLogger()


def save_to_mongo(vs, data, collection_name):
    """
    将数据保存到mongo
    :return:
    """
    collection = db[collection_name]
    uuid = vs.uuid_gen_md5()
    res = collection.find_one({"uuid": uuid})
    if res:
        if res["data"] == data:
            logger.info("[%s 已是最新数据]：%s vs %s %s" % (collection_name, vs.left_name, vs.right_name, uuid))
        else:
            now_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
            collection.update_one({"uuid": uuid}, {"$set": {"uuid": uuid, "data": data, "update_time": now_time}})
            logger.info("[%s 更新成功]：%s vs %s %s" % (collection_name, vs.left_name, vs.right_name, uuid))
    else:
        now_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        collection.insert_one({"uuid": uuid, "left_name": vs.left_name, "right_name": vs.right_name, "competition_time": vs.start_time, "data": data, "update_time": now_time, "create_time": now_time})
        logger.info("[%s 保存成功]：%s vs %s %s" % (collection_name, vs.left_name, vs.right_name, uuid))


# def random_user_agent():
#     """
#     随机生成User-Agent
#     :return:
#     """
#     """准备User-Agent池"""
#     first_num = random.randint(55, 62)
#     third_num = random.randint(0, 3200)
#     fourth_num = random.randint(0, 140)
#     os_type = [
#         '(Windows NT 6.1; WOW64)', '(Windows NT 10.0; WOW64)', '(X11; Linux x86_64)',
#         '(Macintosh; Intel Mac OS X 10_12_6)'
#     ]
#     chrome_version = 'Chrome/{}.0.{}.{}'.format(first_num, third_num, fourth_num)
#
#     user_agent = ' '.join(['Mozilla/5.0', random.choice(os_type), 'AppleWebKit/537.36',
#                            '(KHTML, like Gecko)', chrome_version, 'Safari/537.36']
#                           )
#     return user_agent


@retry(stop_max_attempt_number=3)
def parse_url(url, unicode_escape=""):
    """
    解析url，访问出错三次刷新
    :param url:
    :return:
    """
    res = requests.get(url, headers=config.headers, timeout=8)
    return res.content.decode("unicode_escape") if unicode_escape else res.content.decode()


class NbaVs(object):
    def __init__(self, left_name, right_name, start_time, mid):
        self.left_name = left_name
        self.right_name = right_name
        self.start_time = start_time
        self.mid = mid

    def uuid_gen_md5(self):
        """
        生成標記每場比賽的uuid
        :param string: "老鹰 vs 湖人 2019/11/18 12:30:00"
        :return:
        """
        string = self.left_name + "vs" + self.right_name + self.start_time
        if isinstance(string, str):
            obj = hashlib.md5(string.encode())
        else:
            raise TypeError()
        res = obj.hexdigest()
        return res


def load_log():
    """
    设置日志输出格式
    :param log_level:
    :return:
    """
    # 设置日志的记录等级
    logging.basicConfig(
        level=config.LEVEL,
        format="%(asctime)s %(name)s %(levelname)s %(pathname)s %(message)s ",
        datefmt="%Y-%m-%d  %H:%M:%S %a"
    )  # 调试debug级
    # 创建日志记录器，指明日志保存的路径、每个日志文件的最大大小、保存的日志文件个数上限
    file_log_handler = RotatingFileHandler("logs/log", maxBytes=1024 * 1024 * 100, backupCount=10)
    # 创建日志记录的格式 日志等级 输入日志信息的文件名 行数 日志信息
    formatter = logging.Formatter('%(levelname)s %(filename)s:%(lineno)d %(message)s')
    # 为刚创建的日志记录器设置日志记录格式
    file_log_handler.setFormatter(formatter)
    # 为全局的日志工具对象（flask app使用的）添加日志记录器
    logging.getLogger().addHandler(file_log_handler)