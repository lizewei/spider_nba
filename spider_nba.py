#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Author  : li
# @Email   : wytheli168@163.com
# @Time    : 2019/11/20 21:46
# @Description:
import json
import logging
import re

from selenium import webdriver

import config
from common import parse_url

logger = logging.getLogger()


def spider_nba_text(competition_id, match_id, args_list, data=[]):
    """
    爬取图文直播信息
    :param competition_id:
    :param match_id:
    :param args:
    :return:
    """
    for args in args_list:
        get_text_url = "https://matchweb.sports.qq.com/textLive/detail?competitionId={0}&matchId={1}&AppName=kanbisai&AppOS=iphone&AppVersion=1.0&webVersion=1&ids={2}&callback=textDetail".format(competition_id, match_id, args)
        # res = requests.get(get_text_url, headers=config.headers, timeout=8)
        # text_detail = res.content.decode()
        text_detail = parse_url(get_text_url)
        # print(text_detail)
        text_dict = json.loads(text_detail[11:-2])[1] if json.loads(text_detail[11:-2])[1] else {}
        for id, text in text_dict.items():
            data.append(text)
    data = list(reversed(data))
    return data


def spider_nba_text_args(competition_id, match_id):
    """
    获取请求图文直播所需的参数列表数据
    :return:
    """
    # 获取图文请求所需查询参数的url
    # competition_id = vs_obj.mid.split(":")[0]
    # match_id = vs_obj.mid.split(":")[1]
    get_keys_url = "https://matchweb.sports.qq.com/textLive/index?competitionId={0}&matchId={1}&AppName=kanbisai&AppOS=iphone&AppVersion=1.0&webVersion=1&callback=textIndexCallback".format(competition_id, match_id)
    # 发起get请求，获取请求图文直播所需的参数列表数据
    # res = requests.get(get_keys_url, headers=config.headers, timeout=8)
    # text_index_str = res.content.decode("unicode_escape")
    text_index_str = parse_url(get_keys_url)
    # print(text_index_str)
    text_index_json = json.loads(text_index_str[18:-2])
    keys_list = text_index_json[1]
    # 拼接url参数
    args_list = [",".join(keys_list[i * 20:(i + 1) * 20]) for i in range(0, len(keys_list) // 20 + 1)]
    # 列表反转
    # args_list = list(reversed(args_list))
    return args_list


def spider_nba_homepage(vs_obj):
    """
    爬取直播主页信息
    :return:
    """
    vs_url = "https://sports.qq.com/kbsweb/game.htm?mid=" + vs_obj.mid
    logger.info("[url]：" + vs_url)

    # 启动浏览器，获取网页源代码
    driver = webdriver.PhantomJS(config.PHANTOMJS)
    driver.get(vs_url)
    # 本场概况 --> 每节比赛比分情况
    home_team_list = driver.find_elements_by_xpath('//div[@class="content-wrapper"]//div[@class="host-goals"]/span')
    away_team_list = driver.find_elements_by_xpath('//div[@class="content-wrapper"]//div[@class="guest-goals"]/span')
    # print(home_team_list)
    # print(away_team_list)
    vs_score_data = {}  # 存储比分数据
    if len(home_team_list) == 6:
        home_team = {
            "st1": home_team_list[1].text,
            "nd2": home_team_list[2].text,
            "rd3": home_team_list[3].text,
            "th4": home_team_list[4].text,
            "count": home_team_list[5].text
        }
    else:   # 加时赛
        home_team = {
            "st1": home_team_list[1].text,
            "nd2": home_team_list[2].text,
            "rd3": home_team_list[3].text,
            "th4": home_team_list[4].text,
            "ot": home_team_list[5].text,
            "count": home_team_list[6].text
        }
    vs_score_data[home_team_list[0].text] = home_team

    if len(away_team_list) == 6:
        away_team = {
            "st1": away_team_list[1].text,
            "nd2": away_team_list[2].text,
            "rd3": away_team_list[3].text,
            "th4": away_team_list[4].text,
            "count": away_team_list[5].text
        }
    else:
        away_team = {
            "st1": away_team_list[1].text,
            "nd2": away_team_list[2].text,
            "rd3": away_team_list[3].text,
            "th4": away_team_list[4].text,
            "ot": away_team_list[5].text,
            "count": away_team_list[6].text
        }
    vs_score_data[away_team_list[0].text] = away_team
    # print(vs_score_data)

    vs_result_data = {
        "home_team": home_team_list[0].text,
        "away_team": away_team_list[0].text,
        "competition_time": "",
        "win": home_team if int(home_team_list[-1].text) > int(away_team_list[-1].text) else away_team,
        "lose": home_team if int(home_team_list[-1].text) < int(away_team_list[-1].text) else away_team,
    }
    # print(vs_result_data)

    # 本场概况 --> 本场最佳
    # team_name_both = driver.find_elements_by_xpath('//div[@class="content-wrapper"]//div[@class="logo-section"]//span[@class="team-name to-team boss"]')
    team_name_both = driver.find_elements_by_xpath('//div[@class="content-wrapper"]//div[@class="logo-section"]//span[contains(@class, "team-name")]')
    pog_goal_both = driver.find_element_by_xpath('//div[@class="content-wrapper"]//ul[@class="data-info"]/li[1]')
    pog_assist_both = driver.find_element_by_xpath('//div[@class="content-wrapper"]//ul[@class="data-info"]/li[2]')
    pog_backboard_both = driver.find_element_by_xpath('//div[@class="content-wrapper"]//ul[@class="data-info"]/li[3]')

    pog_data = {}   # 存储本场最佳数据
    pog_goal_both = pog_goal_both.text.split("得分")
    pog_assist_both = pog_assist_both.text.split("助攻")
    pog_backboard_both = pog_backboard_both.text.split("篮板")
    print(pog_goal_both)
    print(pog_assist_both)
    print(pog_backboard_both)
    # 主队最佳分数
    home_team_pog_goal_score = re.search(" [0-9]+", pog_goal_both[0].strip()).group().strip()
    home_team_pog_goal_number_name = re.sub(" [0-9]+", "", pog_goal_both[0].strip()).split("-")
    home_team_pog_goal_name = home_team_pog_goal_number_name[1] if len(home_team_pog_goal_number_name) == 2 else home_team_pog_goal_number_name[0]
    home_team_pog_goal_number = home_team_pog_goal_number_name[0] if len(home_team_pog_goal_number_name) == 2 else ""
    # 主队最佳助攻
    home_team_pog_assist_num = re.search(" [0-9]+", pog_assist_both[0].strip()).group().strip()
    home_team_pog_assist_number_name = re.sub(" [0-9]+", "", pog_assist_both[0].strip()).split("-")
    home_team_pog_assist_name = home_team_pog_assist_number_name[1] if len(home_team_pog_assist_number_name) == 2 else home_team_pog_assist_number_name[0]
    home_team_pog_assist_number = home_team_pog_assist_number_name[0] if len(home_team_pog_assist_number_name) == 2 else ""
    # 主队最佳篮板
    home_team_pog_backboard_num = re.search(" [0-9]+", pog_backboard_both[0].strip()).group().strip()
    home_team_pog_backboard_number_name = re.sub(" [0-9]+", "", pog_backboard_both[0].strip()).split("-")
    home_team_pog_backboard_name = home_team_pog_backboard_number_name[1] if len(home_team_pog_backboard_number_name) == 2 else home_team_pog_backboard_number_name[0]
    home_team_pog_backboard_number = home_team_pog_backboard_number_name[0] if len(home_team_pog_backboard_number_name) == 2 else ""

    # 客场最佳分数
    away_team_pog_goal_score = pog_goal_both[1].strip().split(" ", 1)[0]
    away_team_pog_goal_name = pog_goal_both[1].strip().split(" ", 1)[1] if len(pog_goal_both[1].strip().split(" ", 1)) == 2 else pog_goal_both[1].strip().split(" ", 1)[0]
    away_team_pog_goal_number = pog_goal_both[1].strip().split(" ", 1)[0] if len(pog_goal_both[1].strip().split(" ", 1)) == 2 else ""
    # 客场最佳助攻
    away_team_pog_assist_num = pog_assist_both[1].strip().split(" ", 1)[0]
    away_team_pog_assist_name = pog_assist_both[1].strip().split(" ", 1)[1] if len(pog_assist_both[1].strip().split(" ", 1)) == 2 else pog_assist_both[1].strip().split(" ", 1)[0]
    away_team_pog_assist_number = pog_assist_both[1].strip().split(" ", 1)[0] if len(pog_assist_both[1].strip().split(" ", 1)) == 2 else ""
    # 客场最佳篮板
    away_team_pog_backboard_num = pog_backboard_both[1].strip().split(" ", 1)[0]
    away_team_pog_backboard_name = pog_backboard_both[1].strip().split(" ", 1)[1] if len(pog_backboard_both[1].strip().split(" ", 1)) == 2 else pog_backboard_both[1].strip().split(" ", 1)[0]
    away_team_pog_backboard_number = pog_backboard_both[1].strip().split(" ", 1)[0] if len(pog_backboard_both[1].strip().split(" ", 1)) == 2 else ""

    home_team_pog = {
        "goal": {"score": home_team_pog_goal_score, "name": home_team_pog_goal_name, "number": home_team_pog_goal_number},
        "assist": {"num": home_team_pog_assist_num, "name": home_team_pog_assist_name, "number": home_team_pog_assist_number},
        "backboard": {"num": home_team_pog_backboard_num, "name": home_team_pog_backboard_name, "number": home_team_pog_backboard_number}
    }
    away_team_pog = {
        "goal": {"score": away_team_pog_goal_score, "name": away_team_pog_goal_name, "number": away_team_pog_goal_number},
        "assist": {"num": away_team_pog_assist_num, "name": away_team_pog_assist_name, "number": away_team_pog_assist_number},
        "backboard": {"num": away_team_pog_backboard_num, "name": away_team_pog_backboard_name, "number": away_team_pog_backboard_number}
    }
    pog_data[team_name_both[0].text] = home_team_pog
    pog_data[team_name_both[1].text] = away_team_pog
    # print(pog_data)

    # 技术统计数据解析
    count_data = []
    tab_list = driver.find_elements_by_xpath('//div[@class="tab-content"]//div[@class="skill-content"]//div[contains(@class, "content-row")]')
    # tab_list = tab_list[:-1]  # 多了一行统计
    for row in tab_list:
        span_list = row.find_elements_by_xpath('./div[contains(@class, "content-col")]//span')
        # for span in span_list:
        #     print(span.get_attribute('innerText').strip())
        if span_list[1].get_attribute('innerText').strip() == "统计":
            continue
        temp_dict = {
            "number": span_list[0].get_attribute('innerText').strip(),
            "name": span_list[1].get_attribute('innerText').strip(),
            "role": span_list[2].get_attribute('innerText').strip(),
            "online_duration": span_list[3].get_attribute('innerText').strip(),
            "score": span_list[4].get_attribute('innerText').strip(),
            "backboard": span_list[5].get_attribute('innerText').strip(),
            "assist": span_list[6].get_attribute('innerText').strip(),
            "fgpbase_total": span_list[7].get_attribute('innerText').strip().split("/")[1],
            "fgpbase_goal": span_list[7].get_attribute('innerText').strip().split("/")[0],
            "threeptbas_total": span_list[8].get_attribute('innerText').strip().split("/")[1],
            "threeptbas_goal": span_list[8].get_attribute('innerText').strip().split("/")[0],
            "fta": span_list[9].get_attribute('innerText').strip().split("/")[1],
            "ftm": span_list[9].get_attribute('innerText').strip().split("/")[0],
            "steal": span_list[10].get_attribute('innerText').strip(),
            "block_shot": span_list[11].get_attribute('innerText').strip(),
            "turnover": span_list[12].get_attribute('innerText').strip(),
            "foul": span_list[13].get_attribute('innerText').strip(),
            "record": span_list[14].get_attribute('innerText').strip(),
        }
        count_data.append(temp_dict)
        # for span in div:
        #     print(span.find_element_by_xpath('.//span').text)
        #     print("aaa")
        # print(span)

    # print(team_name_both[0].text, team_name_both[1].text)
    # print(pog_goal_both.text, len(pog_goal_both.text))
    # print(pog_assist_both.text, len(pog_assist_both.text))
    # print(pog_backboard_both.text, len(pog_backboard_both.text))

    # 发起get请求，获取直播主页信息
    # res = requests.get(config.splash_url + "render.html?url=" + vs_url, headers=config.headers, timeout=8)
    # print(config.splash_url + "render.html?url=" + vs_url)
    # temp_dict = {}
    # print(res.content.decode("unicode_escape"))
    #
    # doc = pq(res.content, parser='html')
    # print(doc(".content-wrapper"))

    # xpath解析
    # html = etree.HTML(res.content)
    # home_team_list = html.xpath('//div[@class="content-wrapper"]//div[@class="host-goals"]/span')
    # away_team_list = html.xpath('//div[@class="content-wrapper"]//div[@class="guest-goals"]/span')
    # home_team_list = html.xpath("//div[@id='container']//div[@id='data-box']//div[@class='team-info match-situation-data']//div[@class='period-goals']//div[@class='host-goals']//span/text()")
    # print(home_team_list)
    return vs_score_data, pog_data, count_data, vs_result_data


def spider_nba_mid(mid_list=[]):
    """
    get请求获取mid
    :return:
    """
    get_mid_url = "https://matchweb.sports.qq.com/matchUnion/list?today=2019-11-22&startTime={0}&endTime={1}&columnId=100000&index=3&isInit=true&timestamp=1574245675333&callback=fetchScheduleListCallback100000".format(
        config.START_TIME, config.END_TIME)
    # 发起get请求，获取mid
    # res = requests.get(get_mid_url)
    # fetchs_schedule_str = res.content.decode("unicode_escape")
    fetchs_schedule_str = parse_url(get_mid_url)
    # print(fetchs_schedule_str)
    # print(fetchs_schedule_str[32:-2])
    fetchs_schedule_json = json.loads(fetchs_schedule_str[32:-1])
    for datetime, vs_schedule_list in fetchs_schedule_json["data"].items():
        for vs_schedule in vs_schedule_list:
            mid_list.append(vs_schedule)
    return mid_list


