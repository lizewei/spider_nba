#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Author  : li
# @Email   : wytheli168@163.com
# @Time    : 2019/11/20 18:07
# @Description:

# 获取2019/10/01 -- 2019/11/23日的mid
# https://matchweb.sports.qq.com/matchUnion/list?today=2019-11-22&startTime=2019-10-01&endTime=2019-11-23&columnId=100000&index=3&isInit=true&timestamp=1574245675333&callback=fetchScheduleListCallback100000

# 2019/11/20的vs url
# https://sports.qq.com/kbsweb/game.htm?mid=100000:54431803
# text_keys 请求获取keys列表
# https://matchweb.sports.qq.com/textLive/index?competitionId=100000&matchId=54431803&AppName=kanbisai&AppOS=iphone&AppVersion=1.0&webVersion=1&callback=textIndexCallback
# https://matchweb.sports.qq.com/textLive/detail?competitionId=100000&matchId=54431803&AppName=kanbisai&AppOS=iphone&AppVersion=1.0&webVersion=1&ids=11963368_3626759963,11963367_3512631743,11963366_2076970808,11963365_2179851368,11963364_1908368926,11963360_442706471,11963356_479816278,11963353_2940670366,11963349_572884312,11963342_3127673888,11963338_4257560085,11963337_1723380495,11963334_26024958,11963328_1813330459,11963327_1890076601,11963326_2878742126,11963325_2446964875,11963324_1514821627,11963323_2793109401,11963319_3293359857&callback=textDetail

# mongodb连接对象
from common import save_to_mongo, NbaVs, load_log
from spider_nba import spider_nba_text, spider_nba_text_args, spider_nba_homepage, spider_nba_mid


def main():
    # 配置日志
    load_log()
    # get请求获取mid
    mid_list = spider_nba_mid()

    for mid in mid_list:
        vs = NbaVs(mid["leftName"], mid["rightName"], mid["startTime"], mid["mid"])
        # get请求解析获取直播主页信息
        vs_score_data, pog_data, count_data, vs_result_data = spider_nba_homepage(vs)
        # save到mongo
        save_to_mongo(vs, vs_score_data, "mn_sports_qq_nba_score")
        save_to_mongo(vs, pog_data, "mn_sports_qq_nba_pog")
        save_to_mongo(vs, count_data, "mn_sports_qq_nba_count")
        vs_result_data["competition_time"] = vs.start_time
        save_to_mongo(vs, vs_result_data, "mn_sports_qq_nba_vs")

        competition_id = vs.mid.split(":")[0]
        match_id = vs.mid.split(":")[1]

        # 图文直播解析，入库
        # 获取图文请求所需查询参数的url
        args_list = spider_nba_text_args(competition_id, match_id)
        # 请求获取图文直播信息
        data = spider_nba_text(competition_id, match_id, args_list)
        # save到mongodb
        save_to_mongo(vs, data, "mn_sports_qq_nba_teletext")


if __name__ == "__main__":
    main()










